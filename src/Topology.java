import java.util.LinkedList;
import java.util.List;

public class Topology {
	private final List<Node> nodes;
	private final List<Edge> edges;

	public Topology() {
		this.nodes = new LinkedList<Node>();
		this.edges = new LinkedList<Edge>();
	}

	// getters for the lists

	public List<Node> getNodes() {
		return nodes;
	}

	public List<Edge> getEdges() {
		return edges;
	}
	
	// gets a specific node
	public Node getNodeWithID(String id){
		for(Node n : nodes) if (n.getId().equals(id)) return n;
		System.out.println("failed to return node with id: " + id);
		return null;
	}
	
	// returns all edges that contain a specific node
	public List<Edge> getEdgesWith(String id){
		List<Edge> edgesWithID = new LinkedList<Edge>();
		// add all edges that contain a given edge
		for(Edge e : edges) if (e.containsID(id)) edgesWithID.add(e);
		return edgesWithID;
	}
	
	public Edge getEdgeWith(String id1, String id2){
		for(Edge e : edges) if(e.containsBoth(id1, id2)) return e;
		System.out.println("Failed to return edges with nodes: " + id1 + " and " + id2);
		return null;
	}
	
	public boolean edgeExistsBetween(String id1, String id2){
		for(Edge e : edges) if(e.containsBoth(id1, id2)) return true;
		return false;
	}

	public void setNodeDist(String id, double dist){
		this.getNodeWithID(id).setDist(dist);
	}
	
	public void setNodePrev(String id, String prev){
		this.getNodeWithID(id).setPrev(prev);
	}

	// add the node
	public void addNodeWithID(String id){
		Node n = new Node(id);
		nodes.add(n);
	}

	// removes the node and all edges related to it
	public void removeNodeWithID(String id){
		// remove the node
		nodes.remove(id);

		// remove all edges related to this node
		for (Edge e : edges){
			if (e.containsID(id)) this.removeEdge(e);
		}
	}

	// add the edge
	public void addEdge(Edge e){
		edges.add(e);
	}

	// remove the edge
	public void removeEdge(Edge e){
		edges.remove(e);
	}
}
