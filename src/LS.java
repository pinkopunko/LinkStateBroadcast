import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class LS implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private LinkedList<Edge> cost;


	public LS (String id){
		this.id = id;
		this.cost = new LinkedList<Edge>();
	}


	public String getId() {
		return id;
	}

	public LinkedList<Edge> getCost() {
		return cost;
	}

	public void addLSTableCell(Edge e){
		cost.add(e);
	}

	// removes all cells that have anything to do with a given id
	public void removeCellsOfIdType(String id){
		Edge rmv = null;
		for (Edge e : cost){
			if (id == e.getFrom() || id == e.getTo()){
				rmv = e;
				break;
			}
		}
		cost.remove(rmv);
	}

	// toByteArray and toObject are taken from: http://tinyurl.com/69h8l7x
	public static byte[] lsToByteArray(Object obj) throws IOException {
		byte[] bytes = null;
		ByteArrayOutputStream bos = null;
		ObjectOutputStream oos = null;
		try {
			bos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(bos);
			oos.writeObject(obj);
			oos.flush();
			bytes = bos.toByteArray();
		} finally {
			if (oos != null) {
				oos.close();
			}
			if (bos != null) {
				bos.close();
			}
		}
		return bytes;
	}
	
	public static byte[] convertToBytes(Object object) throws IOException {
	    try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        ObjectOutput out = new ObjectOutputStream(bos)) {
	        out.writeObject(object);
	        return bos.toByteArray();
	    } 
	}
	
	public static Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
	      ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
	      ObjectInputStream is = new ObjectInputStream(bais);
	      bais.close();
	      return is.readObject();
	  }


	// removes all elements from the LS cost table
	public void flush(){
		List<Edge> edgesToRmv = new LinkedList<Edge>();
		for (Edge e : cost) edgesToRmv.add(e);
		cost.removeAll(edgesToRmv);
	}
	
	public void printDetails(){
		for(Edge e : cost){
			System.out.println("Edge from(" + e.getFrom() + ") to(" + e.getTo() + ")");
			System.out.println("Cost = " + e.getCost());
			System.out.println();
		}
	}

}
