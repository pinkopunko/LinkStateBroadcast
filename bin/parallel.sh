#!/bin/sh

#  parallel.sh
#  
#
#  Created by BadBoy17 on 5/10/16.
#

array=( "java Lsr A 2000 configA.txt"
"java Lsr B 2001 configB.txt"
"java Lsr C 2002 configC.txt"
"java Lsr D 2003 configD.txt"
"java Lsr E 2004 configE.txt"
"java Lsr F 2005 configF.txt" )

for cmd in "${array[@]}"
do
    echo "Process \"$cmd\" started"
    $cmd & pid=$!
    PID_LIST+=" $pid"
done

trap "kill $PID_LIST" SIGINT

echo "Parallel processes have started"

wait $PID_LIST

echo
echo "All processes have completed"
