
public class Node {
	private String id;
	private double dist;
	private String prev;
	
	public Node(String id){
		this.id = id;
		this.dist = -1;
	}

	public String getId() {
		return id;
	}

	public double getDist() {
		return dist;
	}

	public void setDist(double dist) {
		this.dist = dist;
	}

	public String getPrev() {
		return prev;
	}

	public void setPrev(String prev) {
		this.prev = prev;
	}
}
