import java.io.Serializable;

public class Edge  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String from;
	private final String to;
	private final int cost; 
	private final int portFrom;
	private final int portTo;

	public Edge(String from, String to, int cost, int portFrom, int portTo) {
		this.from = from;
		this.to = to;
		this.cost = cost;
		this.portFrom = portFrom;
		this.portTo = portTo;
	}

	public boolean containsID(String id){
		// if this edge contains an id return true
		if (id.equals(to) || id.equals(from)) return true;

		// else return false
		return false;
	}
	
	public boolean containsBoth(String id1, String id2){
		if(id1.equals(from) && id2.equals(to)){
			return true;
		} else if(id2.equals(from) && id1.equals(to)){
			return true;
		}
		return false;
	}

	public String getTo() {
		return to;
	}

	public String getFrom() {
		return from;
	}

	public int getCost() {
		return cost;
	} 

	public int getPortTo(){
		return portTo;
	}

	public int getPortFrom(){
		return portFrom;
	}
	
	public void printDetails(){
		System.out.println("From : " + from);
		System.out.println("To : " + to);
		System.out.println("cost : " + cost);
		System.out.println("portFrom : " + portFrom);
		System.out.println("portTo : " + portTo);
	}
} 