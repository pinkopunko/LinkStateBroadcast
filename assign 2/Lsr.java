import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;


public class Lsr {
	// time intervals
	private final static int UPDATE_INTERVAL = 1000;
	private final static int ROUTE_UPDATE_INTERVAL = 30000;

	// the id
	private static String id;
	// port used by this node
	private static int port;
	// config file name
	private static String config;

	private static DatagramSocket socket;

	// the number of neighbors that exist
	private static int numNeighbors;

	//
	private static LinkedList<String> receivedFrom;
	private static LinkedList<NodeFailCheck> tally;

	// creates the topology for this node
	private static Topology topology;

	//
	private static LS linkState;

	public static void main(String[] args) throws SocketException{

		// Get command line arguments.
		if (args.length != 3) {
			// java Lsr A 2000 config.txt
			System.out.println("Required arguments: NODE_ID NODE_PORT CONFIG.TXT");
			return;
		}

		// setup the fields
		id = args[0];
		port = Integer.parseInt(args[1]);
		config = args[2];
		socket = new DatagramSocket(port);
		linkState = new LS(id);

		// read the config file
		setupConfigFile();

		startBroadcast();
	}

	/*
	 * THREADS
	 */
	// Thread for sending our LS every 1 second
	public static Thread sendLS = new Thread(){
		public void run(){
			try { // e catch
				
				//int i = 0; for debug testing
				while(true){
					// Every 1 second
					Thread.sleep(UPDATE_INTERVAL);
					//System.out.println("sendLS Running");
					// for debug testing
//					if(id.equals("B") && i > 0) break;
//					i++;

					// flush our received list so we'll receive from our neighbors and so on
					List<String> flush = new LinkedList<String>();
 					for(String ids : receivedFrom) {
 						flush.add(ids);
					}
 					receivedFrom.removeAll(flush);
					
 					// checks for node failures
					if (!tally.isEmpty()){
						// create two lists to keep track of which nodes to remove from our topology and tally
						List<NodeFailCheck> nodeToRemoveFromTally = new LinkedList<NodeFailCheck>();
						List<Node> nodeToRemoveFromTop = new LinkedList<Node>();
						// iterate through our tally
						for (NodeFailCheck n : tally){
							// if we've recently received from this node, set tally to 0
							if (n.getTally() == -1){
								n.setTally(0);
								//System.out.println("Node -1 at " + n.getId());
							// if we've not heard from this node 3 times, it's busted (remove it)
							} else if (n.getTally() == 2) {
								//System.out.println("Node failure at " + n.getId());
								
								nodeToRemoveFromTally.add(n);
								nodeToRemoveFromTop.add(topology.getNodeWithID(n.getId()));
								
							} else if (n.getTally() >= 0) {
								n.increment();
								//System.out.println("Node inc at " + n.getId());
							} else {
								//System.out.println("wtf");
							}
						}
						
						// remove the nodes that have failed from our linkstate, topology and tally
						// this approach is to avoid concurrency issues
						for(Node n : nodeToRemoveFromTop){
							linkState.removeCellsOfIdType(n.getId());
						}
						tally.removeAll(nodeToRemoveFromTally);
						topology.getNodes().removeAll(nodeToRemoveFromTop);	
						
					}

					// add this info to a UDP packet
					//byte[] linkStateBytes = LS.lsToByteArray(linkState); // e1 catch
					byte[] linkStateBytes = LS.convertToBytes(linkState);
					//System.out.println("size of sendLS = " + linkStateBytes.length);

					
					List<DatagramPacket> packets = new LinkedList<DatagramPacket>();
					
					// for each neighbor
					for(Edge e : topology.getEdges()){
						// Create an UDP datagram and encapsulate the LS packet
						DatagramPacket UDP = new DatagramPacket(linkStateBytes, linkStateBytes.length, InetAddress.getByName("localhost"), e.getPortTo());
						packets.add(UDP);
					}
					
					ListIterator<DatagramPacket> packetIterator = (ListIterator<DatagramPacket>) packets.listIterator();
					while(packetIterator.hasNext()){
						DatagramPacket UDP = packetIterator.next();
						socket.send(UDP);
					}
					
//					ListIterator<Edge> topEdges = (ListIterator<Edge>) topology.getEdges().listIterator();
//					while(topEdges.hasNext()){
//						Edge e = topEdges.next();
//						DatagramPacket UDP = new DatagramPacket(linkStateBytes, linkStateBytes.length, InetAddress.getByName("localhost"), e.getPortTo());
//						socket.send(UDP);
//					}
					
					//if(id.equals("A"))linkState.printDetails();
					

				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	};

	// Thread for receiving
	public static Thread receiveLS = new Thread(){
		public void run(){
			while(true){
				// receive the response from the ping - non-blocking
				try {
					//System.out.println("receiveLS Running");
					// Set up an UPD packet for receiving
					//DatagramPacket response = new DatagramPacket(new byte[14], 14);
					DatagramPacket response = new DatagramPacket(new byte[1500], 1500);
					socket.receive(response);

					// be careful about adding the same edge as the outgoings ones are to incoming edges

					//System.out.println("size of responseLS = " + response.getData().length);
					// unpack the LS packet received
					LS lsr = (LS) LS.convertFromBytes(response.getData()); // catch


					boolean hasReceivedBefore = false;
					// check "receivedFrom" list to see if we've gotten the LS from them
					for (String idR : receivedFrom){
						// if we've received from this particular id set our boolean to true and wait for the next packet to come in
						if (lsr.getId().equals(idR)){
							hasReceivedBefore = true;
							break;
						}
					}

					// skip this packet and don't send it to our neighbors
					if (hasReceivedBefore) continue;
					
					// add it to our list of nodes we've received from
					receivedFrom.add(lsr.getId());
					
					// mark the tally for this node so we know that we've received from them recently
					for(NodeFailCheck n : tally){
						if (n.getId().equals(lsr.getId())){
							n.setTally(-1);
							break;
						}
					}

					// send the LS we receive to our neighbors if it's new to us
					// for each neighbor
					for(Edge e : topology.getEdges()){
						// Create an UDP datagram and encapsulate the LS packet
						DatagramPacket UDP = new DatagramPacket(response.getData(), response.getData().length, InetAddress.getByName("localhost"), e.getPortTo());
						// send the packet
						socket.send(UDP);
					}

					// update topology
					// for each edge inside our received LS packet
					for (Edge e1 : lsr.getCost()){
						// boolean to keep track of if we've encountered a duplicate
						boolean existsInTopology = false;
						// compare with all the edges inside our current topology
						for(Edge e2 : topology.getEdges()){
							// check if we already have this edge in our topology
							// Note: no need to check the cost as we're told that the cost won't change
							if (e1.getFrom().equals(e2.getFrom()) && e1.getTo().equals(e2.getTo())) {
								existsInTopology = true;
								break;
							} else if (e1.getFrom().equals(e2.getTo()) && e1.getTo().equals(e2.getFrom())) {
								existsInTopology = true;
								break;
							}
						}

						// if we haven't encountered this edge before add it to our topology
						if (!existsInTopology) {
							topology.addEdge(e1);
							// check which node we have to add
							boolean toExists = false;
							boolean fromExists = false;
							for (Node node : topology.getNodes()) {
								// check which nodes exists
								if (e1.getFrom().equals(node.getId())){
									fromExists = true;
								} else if (e1.getTo().equals(node.getId())){
									toExists = true;
								} else if (fromExists && toExists){
									break;
								}
							}

							// if we know that both nodes mentioned in the edge don't exist within our topology, then 1 of them must exist
							if (!(fromExists && toExists)){
								// if our "from" node does exist, then add the "to" node
								if (fromExists && !toExists){
									topology.addNodeWithID(e1.getTo());
									NodeFailCheck nfc = new NodeFailCheck(e1.getTo());
									tally.add(nfc);
									// otherwise add the "from" node
								} else if (!fromExists && toExists){
									topology.addNodeWithID(e1.getFrom());
									NodeFailCheck nfc = new NodeFailCheck(e1.getFrom());
									tally.add(nfc);
									// else add both nodes if we've never encountered both
								} else {
									topology.addNodeWithID(e1.getTo());
									topology.addNodeWithID(e1.getFrom());
									NodeFailCheck nfc1 = new NodeFailCheck(e1.getTo());
									tally.add(nfc1);
									NodeFailCheck nfc2 = new NodeFailCheck(e1.getFrom());
									tally.add(nfc2);
									
								}
							}
						}
					}
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	};

	// Thread for computing djikstra's algorithm every 30 seconds
	public static Thread djikstra = new Thread(){
		public void run(){
			try {
				while(true){
					// every 30 seconds
					Thread.sleep(ROUTE_UPDATE_INTERVAL);

					// for testing - every 3 seconds
					//Thread.sleep(3000);

					// create the unchecked list
					List<Node> unchecked = new LinkedList<Node>();
					// add all the nodes from the topology list to the "unchecked" list
					for(Node n : topology.getNodes()) {
						n.setDist(-1);
						if(!n.getId().equals(id))unchecked.add(n);
					}

					// create the checked list for nodes we've covered
					List<Node> checked = new LinkedList<Node>();
					Node selfNode = topology.getNodeWithID(id);
					selfNode.setDist(0);
					checked.add(selfNode);

					// until our unchecked list is empty, loop
					while(!unchecked.isEmpty()){
						double lowestDist = -1;
						Node closest = null;
						// for all checked nodes
						for(Node c : checked){

							// get all unchecked nodes connected to a checked node
							for(Node u : unchecked){
								//System.out.println("checking b/t " + c.getId() + " and " + u.getId());
								// if an edge doesn't exist check the next one
								if (!topology.edgeExistsBetween(c.getId(), u.getId())){
									//System.out.println("doesn't exist");
									//System.out.println();
									continue;
								}
								//System.out.println("does exist");

								Edge CtoU = topology.getEdgeWith(c.getId(), u.getId());

								// if the distance hasn't been set on this unchecked node before, set it
								if(u.getDist() == -1){
									u.setDist(c.getDist() + CtoU.getCost());

									topology.setNodeDist(u.getId(), u.getDist());
									//System.out.println("uncharted. " + u.getId() + "'s distance from source is set to: " + u.getDist());

									// if the distance from origin to u (via c) is less than what 'u' current is, update the distance to the lower value
								} else if (u.getDist() > c.getDist() + CtoU.getCost()){
									u.setDist(c.getDist() + CtoU.getCost());

									topology.setNodeDist(u.getId(), u.getDist());
									//System.out.println("else if");
								}

								// if this is the lowest distance, update it
								if (u.getDist() < lowestDist || lowestDist == -1){
									lowestDist = u.getDist();
									u.setPrev(c.getId());

									topology.setNodeDist(u.getId(), lowestDist);
									topology.setNodePrev(u.getId(), c.getId());

									closest = u;
									//System.out.println("lowest updated to : " + lowestDist);
								} else {
									//System.out.println("lowest wasn't updated as we found : " + u.getDist() + " which is higher than " + lowestDist);
								}
								//System.out.println();
							}
						}
						checked.add(closest);
						unchecked.remove(closest);

						List<String> path = new LinkedList<String>();
						for (Node curr = closest; !curr.getId().equals(id); curr =  topology.getNodeWithID(curr.getPrev())){
							//System.out.println("adding to path list: " + curr.getId());
							// adds to the first
							path.add(0, curr.getId());
						}
						path.add(0, id);

						// print out output here
						System.out.print("least-cost path to node " + closest.getId() + ": ");

						for (String n : path) System.out.print(n);
						System.out.println(" and the cost is " + closest.getDist());

					}

				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	};


	// for debugging purposes
	public static void printTopologyDetails(){
		List<Node> nodeIDs = topology.getNodes();
		List<Edge> edges = topology.getEdges();

		for (Node node : nodeIDs){
			System.out.println("NodeID: " + node.getId());
		}

		for (Edge e : edges){
			e.printDetails();
			System.out.println();
		}
	}

	// starts broadcasting our LS packets to our neighbors
	// send LS pkt every 1 second
	// execute Djikstra's every 30 seconds + print the shortest paths obtained

	/* Handling LS sending/receiving
	 * Initially send LS pkt to all neighbors
	 * 
	 * we receive LS packets and forward them to our neighbors 
	 * we can additionally check the neighbors of who sent the LS packet to us and send to to our NON-common neighbors
	 * reduces redundancy
	 * 
	 * Pkts have a TTL of 9 to ensure that the pkt doesn't continuously circulate through the network
	 * OR
	 * can use a forwarding table to dictate that the LS packets are sent efficiently
	 */
	public static void startBroadcast(){

		// send LS pkts
		sendLS.start();
		// receive LS pkts
		receiveLS.start();
		// compute djikstra - every 30 secounds
		djikstra.start();
	}

	// Initializes the topology:
	// adds itself and all its neighbors from the config file into the topology
	// adds all the edges + their costs mentioned in the config file
	// read the config file
	public static void setupConfigFile(){
		receivedFrom = new LinkedList<String>();
		
		tally = new LinkedList<NodeFailCheck>();

		topology = new Topology();

		// adds itself to the topology
		topology.addNodeWithID(id);

		// To open the file and read it
		File fileToOpen = new File(config);
		try {
			BufferedReader fileContents = new BufferedReader(new FileReader(fileToOpen));
			// get the number of neighbors
			String line = fileContents.readLine();
			numNeighbors = Integer.parseInt(line);

			// setup the topology of neighboring nodes based off config file
			for (int i = 0; i < numNeighbors; i++){
				line = fileContents.readLine();
				Scanner s = new Scanner(line);
				String idTo = s.next();
				int cost = s.nextInt();
				int portTo = s.nextInt();
				// create the edge
				Edge e = new Edge(id, idTo, cost, port, portTo);

				// add the edge and node to the topology
				topology.addNodeWithID(idTo);
				NodeFailCheck nfc = new NodeFailCheck(idTo);
				tally.add(nfc);
				topology.addEdge(e);
				// add the edge to the linkstate
				linkState.addLSTableCell(e);

				// close the scanner
				s.close();
			}

			// close the file reader
			fileContents.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// GETTERS FOR FIELDS
	public static String getId() {
		return id;
	}

	public static int getPort() {
		return port;
	}

	public static String getConfig() {
		return config;
	}

}
