
public class NodeFailCheck {
	private String id;
	private int tally;
	
	public NodeFailCheck(String id){
		this.id = id;
		tally = -1;
	}
	
	public String getId() {
		return id;
	}
	public int getTally() {
		return tally;
	}
	public void setTally(int tally) {
		this.tally = tally;
	}
	public void increment(){
		this.tally++;
	}
	
}
